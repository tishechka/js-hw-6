/*
1) Опишіть своїми словами, що таке метод об'єкту

Метод об'єкта в JavaScript - це функція, яка вкладена в об'єкт і пов'язана з ним.
Він може виконувати певні дії або обробляти дані, що належать до цього об'єкта.

2) Який тип даних може мати значення властивості об'єкта?

Значення властивості об'єкта може мати будь-який тип даних. Наприклад:
Рядки, числа, булеві значення, null, undefined, об'єкти, масиви, функції.

3) Об'єкт це посилальний тип даних. Що означає це поняття?

Об'єкти є посилальним типом даних.
Це означає, що коли об'єкт присвоюється змінній або передається як аргумент функції,
не копіюється сам об'єкт, а лише створюється посилання на нього.
*/


function createNewUser() {
    const newUser = {};

    const firstName = prompt("Enter your first name:");
    const lastName = prompt("Enter your last name:");

    newUser.firstName = firstName;
    newUser.lastName = lastName;

    newUser.getLogin = function() {
        return `${this.firstName.charAt(0).toLowerCase()}${this.lastName.toLowerCase()}`;
    };

    return newUser;
}

const user = createNewUser();
const login = user.getLogin();

console.log(login);
